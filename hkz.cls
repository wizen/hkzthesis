%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% %% <UTF-8>
%% %% 北方民族大学学位论文LaTeX模板 
%% %% 论文样式参考自《北方民族大学研究生学位论文格式和要求--院教字〔2003〕169号》
%% %% 本模板改写自《北京航空航天大学学术论文LaTeX模板》
%% %% 部分样例参考《浙江大学研究生硕士(博士)学位论文LaTeX模板》
%% %% 建议采用等宽字体查看本文档
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 模板标识
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{num}[2018/10/14 v4.0 HKZ thesis class]
\typeout{This is LaTeX template num, Version 4.0 (based on CTex) 2018/10/14, by Wizen Zhang}
\usepackage{graphicx}
\def\HKZThesisVer{v1.0 2023/02/27}
\def\HKZThesis{%
	H%
	{\fontsize{0.8em}{\baselineskip}\selectfont
		\kern-.12em\lower.5ex\hbox{K}%
		\kern-.46em\raise.47ex\hbox{Z}%
	}
	\kern-.35emT%
	\kern-.22em\lower.5ex\hbox{H}%
	\kern-.08em E%
	\kern-.05em\lower.5ex\hbox{S}%
	\kern-.26em I%
	\kern-.26em\raise.5ex\hbox{\rotatebox[origin=c]{180}{S}}%
}

% 引用ctexbook: 五号,1.5倍行距，1.3为与word生成的PDF进行对比所得
\LoadClass[UTF8,zihao=5,linespread=1.3]{ctexbook}

%% 预声明
\RequirePackage{ifthen}       % ifthenelse/equal/isundefined等判断比较命令
\RequirePackage{geometry}     % 设置页边距
\RequirePackage{fancyhdr}     % 设置页眉页脚
\RequirePackage{setspace}     % 设置行间距
\RequirePackage{times}        % Times New Roman字体
\RequirePackage[T1]{fontenc}  % 修改默认文本和数学字形为Times New Roman字体
\RequirePackage{mathptmx}     % 修改默认文本和数学字形为Times New Roman字体
\RequirePackage{float}        % 图片
\RequirePackage{graphicx}     % 图片
%\RequirePackage{subfigure}   % 图片
\RequirePackage{epstopdf}     % 图片
\RequirePackage{subcaption}   % 并排图形。subfigure、subfig与subcaption不兼容
\RequirePackage[stable,perpage,symbol*]{footmisc}% 对脚注样式的控制功能。
\RequirePackage{array}        % 表格处理必备宏包
\RequirePackage{enumitem}     %
\RequirePackage{booktabs}     % 表格上下粗线 
\RequirePackage{longtable}    % 长表格
\RequirePackage{multirow}     % 多行表格
\RequirePackage{tabu}         % 制表宏包，推荐使用，不使用其他制表宏包。
\RequirePackage{tikz}         % 流程图
\RequirePackage{pgfplots}     % 使用pgfplots绘图工具包
\RequirePackage{pgfplotstable}  % 使用pgfplots绘图线性回归包
\RequirePackage{caption}      % 标题设置
\RequirePackage{algorithm2e}  % 算法环境
\RequirePackage{algorithmic}  % 算法环境
\RequirePackage{listings}     % 代码环境
\RequirePackage{amsmath}      % 数学
\RequirePackage{amssymb}      % 数学特殊符号
\RequirePackage{amsthm}       % 定理
\RequirePackage{titletoc}     % 目录
\RequirePackage{remreset}     % 计数器设置
\RequirePackage{hyperref}     % 超连接
\RequirePackage{etoolbox}     % \AtBeginDocument等宏命令
\RequirePackage{pifont}       % 画五角星
\RequirePackage{color}        % To provide color for soul
\RequirePackage{xcolor} 
\RequirePackage{soul}         % To highlight text
\RequirePackage[sort&compress]{natbib}              % BibTex
\DeclareGraphicsExtensions{.eps,.ps,.png,.jpg,.bmp} % 声明使用图像格式
\newcommand{\highlight}[1]{\colorbox{yellow}{#1}}   % 高亮注释
%% 选项
%% 论文类型
\DeclareOption{master}{\gdef\@thesis{master}}             % 学术硕士论文
\DeclareOption{professional}{\gdef\@thesis{professional}} % 专业硕士论文
\DeclareOption{doctor}{\gdef\@thesis{doctor}}             % 博士论文
\DeclareOption{thesis}{\ClassWarning{hkz}{%
		You have not specified the THESIS option. The word thesis should be replaced by one of the following thesis type: master(default), professional, doctor.
}}
%% 打印设置
\DeclareOption{oneside}{\gdef\@printtype{oneside}} % 单面打印 
\DeclareOption{twoside}{\gdef\@printtype{twoside}} % 双面打印


%% 学科设置（文史类、理工类）
\DeclareOption{MA}{\gdef\@subject{MA}} % 文史类
\DeclareOption{MS}{\gdef\@subject{MS}} % 理工类

%% 适应首字母大写
\DeclareOption{Master}{\gdef\@thesis{master}}
\DeclareOption{Professional }{\gdef\@thesis{professional }}
\DeclareOption{Doctor}{\gdef\@thesis{doctor}}
\DeclareOption{Oneside}{\gdef\@printtype{oneside}}
\DeclareOption{Twoside}{\gdef\@printtype{twoside}}

%% 选项传递给ctexbook
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{ctexbook}}

%% 默认选项配置
\ExecuteOptions{master,oneside,blind,MS,a4paper,sub4section}
\setcounter{secnumdepth}{5}
\ProcessOptions\relax

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 数学环境
% 数学符号：
\newcommand\eu{\mathrm{e}}
\newcommand\iu{\mathrm{i}}
\newcommand*{\diff}{\mathop{}\!\mathrm{d}}
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}
% 数学定理：
% 以下定义数学定理环境默认风格为 hkz。
\newtheoremstyle{hkz}%
{0pt}% measure of space to leave above the theorem.
{0pt}% measure of space to leave below the theorem.
{}% name of font to use in the body of the theorem.
{2\ccwd}% measure of space to indent.
{\bfseries}% name of head font.
{.}% punctuation between head and body.
{\ccwd}% space after theorem head; " " = normal interword space
{}% manually specify head (can be left empty, meaning `normal').

\theoremstyle{hkz}
% 定义新的定理
\newcommand{\theoremname}{定理}
\newcommand{\assertionname}{断言}
\newcommand{\axiomname}{公理}
\newcommand{\corollaryname}{推论}
\newcommand{\lemmaname}{引理}
\newcommand{\propositionname}{命题}
\newcommand{\definitionname}{定义}
\newcommand{\examplename}{例}
\newcommand{\remarkname}{注}
\renewcommand{\proofname}{证明}
\renewcommand{\lstlistingname}{代码}
\renewcommand{\algorithmcfname}{算法}


%矩阵（省略号）
\newcommand{\adots}{\mathinner{\mkern2mu%
		\raisebox{0.1em}{.}\mkern2mu\raisebox{0.4em}{.}%
		\mkern2mu\raisebox{0.7em}{.}\mkern1mu}}

%算法输入输出设置
\SetKwInOut{KwData}{输入}
\SetKwInOut{KwResult}{输出}

\newtheorem{theorem}                {\theoremname}     [chapter]
\newtheorem{assertion}  [theorem]   {\assertionname}
\newtheorem{axiom}      [theorem]   {\axiomname}
\newtheorem{corollary}  [theorem]   {\corollaryname}
\newtheorem{lemma}      [theorem]   {\lemmaname}
\newtheorem{proposition}[theorem]   {\propositionname}
\newtheorem{definition}             {\definitionname}  [chapter]
\newtheorem{example}                {\examplename}     [chapter]
\newtheorem*{remark}                {\remarkname}
% From MDPI.cls
%\renewcommand{\qed}{\unskip\nobreak\quad\qedsymbol} %% This places the symbol right after the text instead of placing it at the end on the line.
\renewenvironment{proof}[1][\proofname]{\par %% \proofname allows to have "Proof of my theorem"
	\pushQED{\qed}%
	\normalfont \topsep6\p@\@plus6\p@\relax
	\trivlist
	\item[\hskip\labelsep
	\bfseries %% "Proof" is bold
	#1\@addpunct{.}]\ignorespaces %% Period instead of colon
}{%
	\popQED\endtrivlist\@endpefalse
}

% 代码环境 frame=shadowbox无阴影
\lstset{numbers=left,
	basicstyle=\small\ttfamily,
	numberstyle=\tiny,
	keywordstyle=\color{black}\bfseries,
	commentstyle=\color{black},
	frame=single,
	captionpos=b, 
	rulesepcolor=\color{red!20!green!20!blue!20},
	escapeinside=``,
	xleftmargin=2em,
	xrightmargin=2em, 
	aboveskip=1em}

% 流程图
\usetikzlibrary{arrows,shapes,chains}
%开始结束框
\tikzstyle{startstop} = [rectangle,rounded corners, minimum width=3cm,minimum height=1cm,text centered, draw=black]
%输入输出框
\tikzstyle{io} = [trapezium, trapezium left angle = 70,trapezium right angle=110,minimum width=3cm,minimum height=1cm,text centered,draw=black]
%进程框
\tikzstyle{process} = [rectangle,minimum width=3cm,minimum height=1cm,text centered,text width =3cm,draw=black]
%判断框
\tikzstyle{decision} = [diamond,minimum width=3cm,minimum height=1cm,text centered,draw=black]
%定义箭头
\tikzstyle{arrow} = [thick,->,>=stealth]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 输入
\def\@refcolor{} % 给注释及引用使用颜色 on/off
\newcommand{\refcolor}[1]{\gdef\@refcolor{#1}}
\def\@emptypagewords{} % 空白页
\newcommand{\emptypagewords}[1]{\gdef\@emptypagewords{#1}}
\def\@beginright{} % 摘要、目录、第一章右侧打开 on/off
\newcommand{\beginright}[1]{\gdef\@beginright{#1}}
\newcommand{\Abstract}[2]{\gdef\@abstrctcn{#1} \gdef\@abstrcten{#2}}
\newcommand{\Keyword}[2]{\gdef\@keywordcn{#1} \gdef\@keyworden{#2}}
\newcommand{\Listfigtab}[1]{\gdef\@listfigtab{#1}} % on/off
\newcommand{\Abbreviations}[1]{\gdef\@abbreviations{#1}}


% 封面
\renewcommand*\title[1]{\gdef\HKZ@title{#1}}					% 论文标题（中文）
\newcommand*\englishtitle[1]{\long\gdef\HKZ@englishtitle{#1}}	% 论文标题（英文）
\renewcommand*\author[1]{\gdef\HKZ@author{#1}}					% 作者姓名（中文）
\newcommand*\authoren[1]{\gdef\HKZ@authoren{#1}}				% 作者姓名（英文）
\newcommand*\classification[1]{\gdef\HKZ@classification{#1}}	% 分类号
\newcommand*\serialnumber[1]{\gdef\HKZ@serialnumber{#1}}		% 单位代码
\newcommand*\secretlevel[1]{\gdef\HKZ@secretlevel{#1}}			% 密级
\newcommand*\studentnumber[1]{\gdef\HKZ@studentnumber{#1}}		% 学号
\newcommand*\supervisor[1]{\gdef\HKZ@supervisor{#1}}			% 指导教师（中文）
\newcommand*\supervisoren[1]{\gdef\HKZ@supervisoren{#1}}		% 指导教师（英文）
\newcommand*\applyclass[1]{\gdef\HKZ@applyclass{#1}}		    % 申请的学位门类
\newcommand*\major[1]{\gdef\HKZ@major{#1}}						% 专业名称（中文）
\newcommand*\majoren[1]{\gdef\HKZ@majoren{#1}}			    	% 专业名称（英文）
\newcommand*\class[1]{\gdef\HKZ@class{#1}}				% 研究方向
\newcommand*\institute[1]{\gdef\HKZ@institute{#1}}				% 所在学院
\newcommand*\submitdate[1]{\gdef\HKZ@submitdate{#1}}			% 提交日期（中文）
\newcommand*\submitdateen[1]{\gdef\HKZ@submitdateen{#1}}		% 提交日期（英文）
% 页眉
\newcommand*\session[1]{\gdef\HKZ@session{#1}}			% 毕业届
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  重定义字号命令
\newcommand{\chuhao}{\fontsize{42pt}{\baselineskip}\selectfont}     % 初号
\newcommand{\sanshi}{\fontsize{30pt}{75pt}\selectfont}    % 30号, 2.5倍行距
\newcommand{\xiaochu}{\fontsize{32pt}{38pt}\selectfont}    % 小初
\newcommand{\yihao}{\fontsize{26pt}{36pt}\selectfont}    % 一号, 1.4倍行距
\newcommand{\xiaoyi}{\fontsize{24pt}{36pt}\selectfont}    % 小一号, 1.5倍行距
\newcommand{\erhao}{\fontsize{22pt}{28pt}\selectfont}    % 二号, 1.25倍行距
\newcommand{\xiaoerhao}{\fontsize{18pt}{18pt}\selectfont}    % 小二, 单倍行距
\newcommand{\dasanhao}{\fontsize{18pt}{26pt}\selectfont}    % 大三号, 1.5倍行距
\newcommand{\sanhao}{\fontsize{16pt}{24pt}\selectfont}    % 三号, 1.5倍行距
\newcommand{\sansan}{\fontsize{16pt}{32pt}\selectfont}    % 三号, 3倍行距
\newcommand{\xiaosan}{\fontsize{15pt}{22pt}\selectfont}    % 小三, 1.5倍行距
\newcommand{\sihao}{\fontsize{14pt}{21pt}\selectfont}    % 四号, 1.5倍行距
\newcommand{\sierwu}{\fontsize{14pt}{35pt}\selectfont}    % 四号, 2.5倍行距
\newcommand{\banxiaosi}{\fontsize{13pt}{19.5pt}\selectfont}% 半小四, 1.5倍行距
\newcommand{\xiaosi}{\fontsize{12pt}{18pt}\selectfont}    % 小四, 1.5倍行距
\newcommand{\xiaosier}{\fontsize{12pt}{24pt}\selectfont}    % 小四, 2倍行距
\newcommand{\dawuhao}{\fontsize{11pt}{11pt}\selectfont}    % 大五号, 单倍行距
\newcommand{\wuhao}{\fontsize{10.5pt}{10.5pt}\selectfont}    % 五号, 单倍行距
\newcommand{\xiaowuhao}{\fontsize{9pt}{9pt}\selectfont}   %小五号，单倍行距

% 宋体的中文加粗 
\setCJKfamilyfont{zhsong}[AutoFakeBold = {2.17}]{SimSun}
\renewcommand*{\songti}{\CJKfamily{zhsong}}

% 仿宋_GB2312 
\setCJKfamilyfont{fs}{FangSong_GB2312}
\newcommand{\fs}{\CJKfamily{fs}}

% 定义封面及标题页设置中常用的下划线命令（默认宽度90pt）
\renewcommand\ULthickness{0.7pt}			% 重新定义下划线的厚度
\newcommand\HKZunderline[2][90pt]{\uline{\hbox to #1{\hss#2\hss}}\hskip3pt}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% CTeX调整
% 半角/缩进/行伸缩设置
\ctexset{%
	punct=banjiao, % 半角
	autoindent=2,  % 缩进
	linestretch=1, % 行伸缩
}
% 部分标题修改
\ctexset{%
	contentsname={目~~~~录},
	listfigurename={图~清~单},
	listtablename={表~清~单},
}

\renewcommand{\contentsname}{\xiaoerhao \bfseries{\songti 目~~~~录}}

% 标题样式修改
\ifthenelse{\equal{\@subject}{MS}}{% 理工类
	\ctexset{%设置标题的格式
		chapter={%章标题 : 一级标题用宋体小三号，加粗，居中
			format=\xiaosan \bfseries \songti \centering \thispagestyle{fancy},%此页添加页眉页脚
			name={},number={\arabic {chapter}},
			fixskip=true,
			beforeskip=4pt,%与word生成的PDF进行对比所得
			afterskip=28pt,%与word生成的PDF进行对比所得
			break=\clearpage,
		},
		section={%节标题 : 二级标题用宋体四号，加粗，左顶格
			format=\sihao \bfseries \songti \raggedright,
			fixskip=true,
			beforeskip=28pt,%与word生成的PDF进行对比所得
			afterskip=24pt,%与word生成的PDF进行对比所得
		},
		subsection={%条标题 : 三级及以下标题用宋体小四号，加粗，左顶格
			format=\xiaosi \bfseries \songti \raggedright,
			fixskip=true,
			beforeskip=24pt,%与word生成的PDF进行对比所得
			afterskip=20pt,%与word生成的PDF进行对比所得
		},
		subsubsection={% 附加4级标题 : 五号宋体居左
			format=\xiaosi \bfseries \songti \raggedright,
			fixskip=true,
			beforeskip=20pt,
			afterskip=20pt,
		},
		paragraph={% 附加5级标题 : 五号宋体居左
			format=\xiaosi \bfseries \songti \raggedright,
			fixskip=true,
			beforeskip=20pt,
			afterskip=20pt,
		},
		subparagraph={% 附加6级标题 : 五号宋体居左
			format=\xiaosi \bfseries \songti \raggedright,
			fixskip=true,
			beforeskip=20pt,
			afterskip=20pt,
		}
	}
}{
	\ifthenelse{\equal{\@subject}{MA}}{% 文史类
		\ctexset{%设置标题的格式
			chapter={%章标题 : 三号宋体居中 3倍行间距 另起新页
				format=\xiaosan \bfseries \songti \centering \thispagestyle{fancy},%此页添加页眉页脚
				fixskip=true,
				beforeskip=4pt,%与word生成的PDF进行对比所得
				afterskip=28pt,%与word生成的PDF进行对比所得
				break=\clearpage,
			},
			section={%节标题 : 四号宋体居左 2.5倍行间距
				format=\sihao \bfseries \songti \centering,
				name = {第,节},
				number = \chinese{section},
				fixskip=true,
				beforeskip=28pt,%与word生成的PDF进行对比所得
				afterskip=24pt,%与word生成的PDF进行对比所得
			},
			subsection={%条标题 : 小四号宋体居左 2倍行间距
				format=\xiaosi \bfseries \songti \raggedright,
				name = {\hspace{2em},、},%首行缩进2字符
				number = \chinese{subsection},
				fixskip=true,
				beforeskip=24pt,%与word生成的PDF进行对比所得
				afterskip=20pt,%与word生成的PDF进行对比所得
			},
			subsubsection={% 附加4级标题 : 五号宋体居左
				format=\wuhao \songti \raggedright,
				name = {\hspace{2em}（,）},%首行缩进2字符
				number = \chinese{subsubsection},
				fixskip=true,
				beforeskip=20pt,
				afterskip=20pt,
			},
			paragraph={% 附加5级标题 : 五号宋体居左
				format=\wuhao \songti \raggedright,
				name = {\hspace{2em},.},%首行缩进2字符
				number = \arabic{paragraph},
				fixskip=true,
				beforeskip=20pt,
				afterskip=20pt,
			},
			subparagraph={% 附加6级标题 : 五号宋体居左
				format=\wuhao \songti \raggedright,
				name = {\hspace{2em}（,）},%首行缩进2字符
				number = \arabic{subparagraph},
				fixskip=true,
				beforeskip=20pt,
				afterskip=20pt,
			}
		}
	}{}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 页面设置
% 设置页面大小
% A4纸页边距均25mm
% 打印留边平移5mm
\geometry{papersize={210mm,297mm}}
\ifthenelse{\equal{\@printtype}{twoside}}{% 双面打印
	\geometry{left=30mm,right=25mm,top=30mm,bottom=25mm}
}{%
	\ifthenelse{\equal{\@printtype}{oneside}}{% 单面打印
		\geometry{left=25mm,right=25mm,top=30mm,bottom=25mm}
		\geometry{layouthoffset=5mm}}{}
}

% 设置空白页眉页脚
\pagestyle{fancy}
\fancyhf{}%清空初始页眉页脚


% 目录显示设置：\titlecontents{章节名称}[左端距离]{标题字体、与上文间距等}{标题序号}{空}{引导符和页码}[与下文间距]
\titlecontents{chapter}[0pt]{\bfseries \songti\zihao{4}}{\thecontentslabel\ }{}
{\hspace{.5em}\titlerule*[4pt]{$\cdot$}\normalsize\contentspage}
\titlecontents{section}[2em]{\songti\zihao{4}}{\thecontentslabel\ }{}
{\hspace{.5em}\titlerule*[4pt]{$\cdot$}\normalsize\contentspage}
\titlecontents{subsection}[3em]{\songti\zihao{4}}{\thecontentslabel\ }{}
{\hspace{.5em}\titlerule*[4pt]{$\cdot$}\normalsize\contentspage}
\titlecontents{figure}[0pt]{\songti\zihao{-4}}{图~\thecontentslabel\quad}{}
{\hspace{.5em}\titlerule*[4pt]{$\cdot$}\normalsize\contentspage}
\titlecontents{table}[0pt]{\songti\zihao{-4}}{表~\thecontentslabel\quad}{}
{\hspace{.5em}\titlerule*[4pt]{$\cdot$}\normalsize\contentspage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 浮动体相关定义
% 设置浮动环境标题的字体大小。根据学位论文格式要求，插图和表格标题字体需要比正文
% 字体略小。
\captionsetup[figure]{
	position=bottom, margin=0mm, format=hang,
	labelsep=quad,		% 去掉图表号后的冒号。图序与图名文字之间空一个汉字宽度。
	skip=9pt,			% 标题与图标之间空9pt距离。
	font=small,		
	aboveskip=9pt, belowskip=-9pt, justification=centerlast}
\captionsetup[subfigure]{belowskip=0pt}
\captionsetup[table]{
	position=top, margin=0mm, format=hang,
	labelsep=quad,		% 去掉图表号后的冒号。图序与图名文字之间空一个汉字宽度。
	skip=9pt,			% 标题与图标之间空9pt距离。
	font=small,
	aboveskip=9pt, belowskip=9pt}
\newcommand{\tabincell}[2]{\begin{tabular}{@{}#1@{}}#2\end{tabular}} % 单元格换行
%% 以下为设置浮动对象和文字之间的距离。
% 版心顶部或者版心底部浮动体之间的垂直距离，默认为12pt plus 2pt minus 2pt。
\setlength{\floatsep}{12bp \@plus4pt \@minus1pt}

% 文本行之间浮动体（使用h位置选项）与上下文之间的距离，默认为12pt plus 2pt minus
% 2pt。
\setlength{\intextsep}{4bp \@plus4pt \@minus2pt}

% 版心顶部或底部的浮动体与文本之间的距离，默认为12pt plus 2pt minus 2pt。
\setlength{\textfloatsep}{12bp \@plus4pt \@minus2pt}

% 浮动页中，版心顶部的浮动体与版心顶边之间的距离，默认为0pt plus 1fil。
\setlength{\@fptop}{0bp \@plus1.0fil}

% 浮动页中浮动体直接的距离，默认为8pt plus 2fil。
\setlength{\@fpsep}{8bp \@plus2.0fil}

% 浮动页中，版心底部的浮动体与版心底边之间的距离，默认为0pt plus 1fil。
\setlength{\@fpbot}{0bp \@plus1.0fil}

%% 下面这组命令使浮动对象的缺省值稍微宽松一点，从而防止幅度对象占据过多的文本页
%% 面，也可以防止在很大空白的浮动页上放置很小的图形。
% 默认情况下，LaTeX要求每页的文字至少占据 20%，否则该页就只单独放置一个浮动环境。
% 而这通常不是我们想要的。我们将这个要求降低到 5%。
\renewcommand{\textfraction}{0.05}
% 在一文本页中，被置于版心顶部的所有浮动体的高度与版心高度的最大比值，默认为0.7，
% 这里修改为0.7，任何造成高度超过版心高度70% 的浮动体都被将阻止置于当前版心顶部。
\renewcommand{\topfraction}{0.7}
% 同上，只不过变成了版心底部，默认为0.3.
\renewcommand{\bottomfraction}{0.5}
% 在任意一个“浮动页”中，所有的浮动体高度与版心的高度的最小比值，默认为 0.5，这里
% 设置成0.85。因此，浮动页中空白与版心的比值不会超过1-\floatpagefraction。
\renewcommand{\floatpagefraction}{0.0}

%设置“图1.1”、“表2.2”为“图1-1”、“表2-2”
\renewcommand {\thetable} {\thechapter{}-\arabic{table}}
\renewcommand {\thefigure} {\thechapter{}-\arabic{figure}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ---------- 脚注样式 ----------
% 使用footmisc宏包和pifont宏包设置符合 GB/T 7713.1-2006 规范的脚注样式。注意，由
% 于pifont宏包提供的特殊符号的限制，一页之中最多只能有10个脚注。
% 这些命令设置跟脚注设置方法思路全都引用自南京大学计算机科学与技术系胡海星编写的
% 《NJU-Thesis:南京大学学位论文XeLaTeX模板》v1.1.18 (2015/7/16)版本。
\DefineFNsymbols*{circlednumber}[text]{%
	{\ding{192}} %
	{\ding{193}} %
	{\ding{194}} %
	{\ding{195}} %
	{\ding{196}} %
	{\ding{197}} %
	{\ding{198}} %
	{\ding{199}} %
	{\ding{200}} %
	{\ding{201}}}
\setfnsymbol{circlednumber}
% 脚注部分需要修改，段落部分单倍行距。
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 设置参考文献格式
\newcommand\bibstyle@super{\bibpunct{[}{]}{,}{s}{,}{\textsuperscript{,}}}
\newcommand\bibstyle@numbers{\bibpunct{[}{]}{,}{n}{,}{,}}
\newcommand\bibstyle@authoryear{\bibpunct{(}{)}{;}{a}{,}{,}}
\bibpunct{[}{]}{,}{n}{}{}
\setlength{\bibsep}{0pt}
\newcommand{\upcite}[1]{\textsuperscript{\cite{#1}}}

% 设置PDF
\hypersetup{%
	colorlinks=true,
	bookmarksnumbered=true,
	bookmarksopen=true,
	pdffitwindow=true, 
	pdfsubject={HKZ thesis},
	pdfcreator={LaTeXed~By~Wizen Zhang(https://github.com/WizenZhang/HKZThesis)}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\clearautopage}{%右侧起新页
	\clearpage
	\ifthenelse{\equal{\@printtype}{twoside}}{% 双面
		\ifodd\c@page 
		\else
		\hbox{}
		\vspace*{\fill}
		\begin{center}
			{\textcolor[rgb]{0.75,0.75,0.75}{\@emptypagewords}}
		\end{center}
		\vspace{\fill}
		\thispagestyle{empty}%此页不加页眉页脚
		\newpage
		\if@twocolumn \hbox{} \newpage \fi
		\fi
	}{}
}
\newcommand{\sethyper}{%设置连接应用颜色
	\hypersetup{%
		pdftitle={\HKZ@title},
		pdfkeywords={\@keywordcn},
		pdfauthor={\HKZ@author}
	}
	\ifthenelse{\equal{\@refcolor}{on}}{%
		\hypersetup{
			allcolors=blue,
		}
	}{%
		\hypersetup{
			allcolors=black,
		}
	}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% AfterPreamble = AtBeginDocument
% AtEndPreamble -> AtBeginDocument -> AfterEndPreamble -> AtEndDocument -> AfterEndDocument
\AtBeginDocument{%
	\sethyper
}
\AfterEndPreamble{%

	\makecovercn
	\makecopyrighthkzd
	\makestatementhkzd %独创性声明
	
	\def\headrule{}%此页不加页眉
	\ifthenelse{\equal{\@printtype}{twoside}}{% 双面打印
		\fancyfoot[C]{}
		\fancyfoot[RO]{\zihao{-5} \uppercase\expandafter{\romannumeral \thepage}}
		\fancyfoot[LE]{\zihao{-5} \uppercase\expandafter{\romannumeral \thepage}}
	}{%
		\fancyfoot[C]{\zihao{-5} \uppercase\expandafter{\romannumeral \thepage}}
	}
	
	
	\setcounter{page}{-9} %目录页码不显示且不超过9页
	\makecontextlist   %目录
	
	
	\def\headrule{%页眉底部长条的样式
		{\hrule\@height\textwidth height 0.3mm\@width\textwidth\vskip0.35mm%上条宽0.3mm，两条之间的空白0.35mm
			\hrule\@height\textwidth height 0.3mm\@width \textwidth\vskip 0pt}}%下条宽0.3mm
	
	\renewcommand{\headrulewidth}{0.4pt}%页眉设置
	\ifthenelse{\equal{\@printtype}{twoside}}{% 双面打印
		\fancyfoot[C]{}
		\fancyfoot[RO]{\zihao{5} \thepage}
		\fancyfoot[LE]{\zihao{5} \thepage}
	}{%
		\fancyfoot[C]{\zihao{5} \thepage}
	}
	\ifthenelse{\equal{\@thesis}{master} \OR \equal{\@thesis}{professional}}{%
		\fancyhead[C]{\zihao{-5}河南科技职业大学本科毕业论文（设计）}
	}{}
	\ifthenelse{\equal{\@thesis}{doctor}}{%
		\fancyhead[C]{\zihao{-5}河南科技职业大学本科毕业论文（设计）}
	}{}
	\setcounter{page}{1}
}


%% 制作封面及声明
\newcommand{\makecovercn}{% 中文封面
	{\pdfbookmark[0]{中文封面}{makecovercn}
		\thispagestyle{empty}%此页不加页眉页脚
		
		
		\vspace{20ex}
		\begin{center}
			{\sanshi \songti {河南科技职业大学}}\par
			\vspace{18ex}
			{\xiaoyi \songti {{\HKZ@session}届本科毕业论文（设计）}}\par	
		\end{center}		
		\begin{spacing}{1.4}%1.4倍行距，与word生成的PDF进行对比所得
			\vspace{5ex}
			\begin{center}
				{\erhao \fangsong {\HKZ@title\hfill}\\}
				%	{\erhao{\HKZ@englishtitle\hfill}\\}
			\end{center}
			\vspace{5ex}
			\centering\dasanhao\fangsong\hspace{1em}
			\begin{tabular}{ccl@{\extracolsep{2em}}l}
				~~ & 姓\hfill 名:& \underline{\makebox[13em]{\qquad {\HKZ@author} \qquad }}\\
				~~&学\hfill 号:&\underline{\makebox[13em]{\qquad {\HKZ@studentnumber} \qquad }}\\
				~~ & 专\hfill 业:& \underline{\makebox[13em]{\qquad {\HKZ@major} \qquad }}\\
				~~ & 班\hfill 级:& \underline{\makebox[13em]{\qquad {\HKZ@class} \qquad }}\\
				~~ & 指导教师:& \underline{\makebox[13em]{\qquad {\HKZ@supervisor} \qquad }}\\
				~~ & 教学单位:& \underline{\makebox[13em]{\qquad {\HKZ@institute} \qquad }}\\
			\end{tabular}
			
			\vfill
		\end{spacing}	
		\parbox[t][2cm][b]{\textwidth}{{\centering\sansan\fangsong \quad\enspace
				\hspace{10em} \HKZ@submitdate}}	
		\vspace*{6ex}
		\clearautopage
}}

\newcommand{\makecopyrighthkzd}{% 河科职大版权
	{\pdfbookmark[0]{河科职大版权}{makestatementhkz}
		\thispagestyle{empty}%此页不加页眉页脚
		
		\begin{spacing}{1.3}%1.5倍行距，1.3为与word生成的PDF进行对比所得
			
			\begin{center}	
				
				\erhao \heiti 河南科技职业大学\par 毕业论文（设计）版权使用授权书
				
				\parbox[t][12cm][c]{\textwidth}{\dasanhao\fangsong
					\hspace{2em}本人完全了解河南科技职业大学关于收集、保存和使用学位毕业论文（设计）的规定，同意如下各项内容：按照学校要求提交毕业论文（设计）的印刷本和电子版本；学校有权保存毕业论文（设计）的印刷本和电子版，并采用影印、缩印、扫描、数字化或其它手段保存毕业论文（设计）；学校有权提供目录检索以及提供本毕业论文（设计）全文或者部分的阅览服务；学校有权按有关规定向国家有关部门或者机构送交毕业论文（设计）的复印件和电子版；在不以赢利为目的的前提下，学校可以适当复制毕业论文（设计）的部分或全部内容用于学术活动。}
				\vfill
				
				\parbox[t][2cm][b]{\textwidth}{{\dasanhao\fangsong \quad\enspace
						\hspace{8em}毕业论文（设计）作者签名：}}
				\parbox[t][1cm][b]{\textwidth}{{\dasanhao\fangsong \quad\enspace
						\hspace{17em}年\hspace{1.5em}月\hspace{1.5em}日}}	
			\end{center}
			
		\end{spacing}
		\clearautopage
}}

\newcommand{\makestatementhkzd}{% 河科职大声明
	{\pdfbookmark[0]{河科职大声明}{makestatementhkz}
		\thispagestyle{empty}%此页不加页眉页脚
		\begin{spacing}{1.3}%1.5倍行距，1.3为与word生成的PDF进行对比所得
			\begin{center}	
				
				\erhao \heiti 河南科技职业大学\par 毕业论文(设计)原创性声明
				
				\parbox[t][8cm][c]{\textwidth}{\dasanhao\fangsong
					\hspace{2em}本人郑重声明：所呈交的毕业论文（设计），是本人在指导教师指导下，进行研究工作所取得的成果。除文中已经注明引用的内容外，本毕业论文（设计）的研究成果不包含任何他人创作的、已公开发表或者没有公开发表的作品的内容。对本毕业论文（设计）所涉及的研究工作做出贡献的其他个人和集体，均已在文中以明确方式标明。本学位毕业论文（设计）原创性声明的法律责任由本人承担。}
				\vfill
				
				\parbox[t][2cm][b]{\textwidth}{{\dasanhao\fangsong \quad\enspace
						\hspace{8em}毕业论文（设计）作者签名：}}
				\parbox[t][1cm][b]{\textwidth}{{\dasanhao\fangsong \quad\enspace
						\hspace{17em}年\hspace{1.5em}月\hspace{1.5em}日}}	
			\end{center}
			
		\end{spacing}
		\clearautopage
}}
%% 中文摘要环境
\newenvironment{abstractcn}{\chapter*{\xiaoerhao \bfseries{\songti  摘~~~~要}}
	%\pagenumbering{Roman} %设置罗马数字页码
	\addcontentsline{toc}{chapter}{\abstractname}
}{\if@twocolumn\else\null\fi}
% 英文摘要环境
\newenvironment{abstracten}{\chapter*{\xiaoerhao \bfseries ABSTRACT}
	\addcontentsline{toc}{chapter}{ABSTRACT}
}{\if@twocolumn\else\null\fi}

\newcommand{\abstructkeyword}{% 摘要
	{\pdfbookmark[0]{摘要}{abstructkeyword}
		\begin{abstractcn}
			{\xiaosi \songti ~\par
				\@abstrctcn \par ~\par
				\noindent {\xiaosi \bfseries{\songti 关键词：}} \@keywordcn %关键词行不缩进
			}
		\end{abstractcn}
		
		\newpage
		\begin{abstracten}
			{\xiaosi  ~\par
				\@abstrcten \par ~\par
				\noindent {\xiaosi \bfseries Key words:} \@keyworden %关键词行不缩进
			}
		\end{abstracten}
		
		\ifthenelse{\equal{\@beginright}{off}}{\clearpage}{\clearautopage}
	}\clearpage}

\newcommand{\makecontextlist}{% 目录
	{\pdfbookmark[0]{目录}{makecontextlist}
		\tableofcontents
		\ifthenelse{\equal{\@beginright}{off}}{\clearpage}{\clearautopage}
}}

\renewcommand{\baselinestretch}{1.85}%伸展因子，1.5倍行距，1.3为与word生成的PDF进行对比所得
\setlength{\baselineskip}{10pt} %行基线间距。
\setlength{\lineskiplimit}{1pt}%当两行字之间的距离小于\lineskiplimit时，行距自动设为\lineskip。
\setlength{\parskip}{0pt}%部分段间距。

\newcommand{\chaptera}[1]{%无章号但编入目录
	\ctexset{chapter/numbering=false,chapter/format=\zihao{3} \bfseries \songti \centering
		\thispagestyle{fancy}}%此页添加页眉页脚
	\chapter{#1}
}


\newcommand{\reference}{%参考文献
	\ctexset{chapter/numbering=false,chapter/format=\zihao{3} \bfseries \songti \centering
		\thispagestyle{fancy}}%此页添加页眉页脚
	
	\ifthenelse{\equal{\@printtype}{twoside}}{% 双面打印
		\cleardoublepage
	}{%
		\clearpage
	}	
	\phantomsection%使用了hyperref宏包来生成PDF文件的书签链接
	\addcontentsline{toc}{chapter}{参考文献}
}

\renewcommand{\appendix}{% 附录
	\ctexset{chapter/numbering=false,chapter/format=\zihao{3} \bfseries \songti \centering
		\thispagestyle{fancy}}%此页添加页眉页脚
	\chapter{附录}
	\setcounter{section}{0}%
	\setcounter{subsection}{0}%
	\setcounter{subsubsection}{0}%
	%
	\gdef\thesection{\@Alph\c@section}%
	\gdef\thesubsection{\@Alph\c@section.\@arabic\c@subsection}%
	%
	
	\renewcommand{\thefigure}{A\arabic{figure}}
	\setcounter{figure}{0}
	\renewcommand{\thetable}{A\arabic{table}}
	\setcounter{table}{0}
	\renewcommand{\theequation}{A\arabic{equation}}
	\setcounter{equation}{0}
}

\newcommand{\acknowledgments}{%致谢	
	\ctexset{chapter/numbering=false,chapter/format=\zihao{3} \bfseries \songti \centering
		\thispagestyle{fancy}}%此页添加页眉页脚
		\chapter{致谢}
}


%% --END--
\endinput