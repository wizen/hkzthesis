# 河南科技职业大学学位论文LaTeX模板
[![](https://img.shields.io/badge/version-v1.0-brightgreen.svg)](https://gitee.com/wizen/hkzthesis)   ![](https://img.shields.io/badge/license-MIT-blue.svg)

**本项目为河南科技职业大学学位论文模板HKZThesis。根据《河南科技职业大学毕业论文（设计）工作条例》规定，为统一学校毕业论文（设计）写作与排版打印格式，特制定本模板。本模板为个人兴趣之作，仅供学习交流使用。**


## 预览

**PDF下载预览:**[HKZThesis.pdf](https://gitee.com/wizen/hkzthesis/blob/master/HKZThesis.pdf)
 

## 下载地址

+ [GitHub](https://gitee.com/wizen/hkzthesis): [https://gitee.com/wizen/hkzthesis](https://gitee.com/wizen/hkzthesis)

## 模板使用

**LaTeX: 参看LaTeX模板示例HKZThesis.tex及相应插入章节tex/*.tex**

### 模式切换

\documentclass[<*subject*>]{hkz}

##### I.论文学科类型(subject)
+ 理工类（MS）<缺省值>
+ 文史类（MA）

## 项目相关

### 项目环境

![](https://img.shields.io/badge/Windows%2010-64bit-blue.svg)   ![](https://img.shields.io/badge/TeXstudio-2.12.16-orange.svg)   ![](https://img.shields.io/badge/Texlive2019-20190410-ff69b4.svg)

### 项目结构

```
HKZThesis
 |- HKZThesis.tex         // LaTeX模板运行入口(main)
 |- HKZThesis.pdf         // PDF模板样例
 |- hkz.cls               // LaTeX宏模板文件
 |- GBT7714-2005.bst      // 国标参考文献BibTeX样式文件2005版
 |- GBT7714-2015.bst      // 国标参考文献BibTeX样式文件2015版
 |- tex/*.tex             // LaTeX模板样例中的独立章节
 |- figures/*             // LaTeX模板样例中的插图存放目录
 |- ref.bib               // LaTeX模板中的参考文献Bib文件
 |- make.bat              // 生成HKZThesis.pdf
 |- clean.bat             // 清理冗余文件
 +- ReadMe.md             // 本文件
```

## 友情链接

+ Windows系统可以选择TexLive+TeXstudio的方式安装，配置教程请参看百度经验：
[https://jingyan.baidu.com/article/b2c186c83c9b40c46ff6ff4f.html](https://jingyan.baidu.com/article/b2c186c83c9b40c46ff6ff4f.html)

+ LaTeX入门视频教程请参看CSDN：
[https://blog.csdn.net/so_geili/article/details/51702564](https://blog.csdn.net/so_geili/article/details/51702564)


## 建议及问题反馈

+ E-mail: [wizen_zhang@163.com](wizen_zhang@163.com)
+ Gitee: [https://gitee.com/wizen/hkzthesis/issues](https://gitee.com/wizen/hkzthesis/issues)

## 致谢

感谢[WeiQM](https://github.com/CheckBoxStudio/BUAAThesis)开源的《北航研究生学位论文模板》（Word+LaTeX），为本项目LaTeX模板的形成提供了很大的帮助。

***

By [Wizen Zhang](https://gitee.com/wizen), at HKZ.